# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the converter package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: converter\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-25 11:47-0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/io.gitlab.adhami3310.Converter.desktop.in:3
#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:5
#: converter/gtk/window.blp:9 converter/gtk/window.blp:42
msgid "Converter"
msgstr ""

#: data/io.gitlab.adhami3310.Converter.desktop.in:4
msgid "Convert"
msgstr ""

#: data/io.gitlab.adhami3310.Converter.desktop.in:5
msgid "Convert and Manipulate images"
msgstr ""

#: data/io.gitlab.adhami3310.Converter.desktop.in:11
msgid "image;convert;converting;processing"
msgstr ""

#: data/io.gitlab.adhami3310.Converter.gschema.xml:6
msgid "Shows less popular datatypes in the menu"
msgstr ""

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:6
msgid "Khaleel Al-Adhami"
msgstr ""

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:7
msgid "Convert and manipulate images"
msgstr ""

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:13
msgid ""
"A simple and easy-to-use Flatpak application built with libadwaita for "
"converting and manipulating images."
msgstr ""

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:26
msgid "image"
msgstr ""

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:27
msgid "convert"
msgstr ""

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:28
msgid "converting"
msgstr ""

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:29
msgid "processing"
msgstr ""

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:45
msgid "Overview"
msgstr ""

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:65
msgid "Added batch processing and support for GIF, PDF, TIFF, and ICO."
msgstr ""

#: converter/file_chooser.py:67
#, python-brace-format
msgid "’{input_ext}’ is not supported"
msgstr ""

#: converter/file_chooser.py:90
msgid "Select an image"
msgstr ""

#: converter/file_chooser.py:97
msgid "Supported image files"
msgstr ""

#: converter/file_chooser.py:114
msgid "No file extension was specified"
msgstr ""

#: converter/file_chooser.py:120
#, python-brace-format
msgid "’{file_ext}’ is of the wrong format"
msgstr ""

#: converter/file_chooser.py:129
msgid "Select output location"
msgstr ""

#: converter/gtk/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: converter/gtk/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr ""

#: converter/gtk/help-overlay.blp:19
msgctxt "shortcut window"
msgid "Quit"
msgstr ""

#: converter/gtk/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Open Image"
msgstr ""

#: converter/gtk/window.blp:53
msgid "_Open Image…"
msgstr ""

#: converter/gtk/window.blp:62
msgid "…or just drop it"
msgstr ""

#: converter/gtk/window.blp:89
msgid "Drop the image to load it"
msgstr ""

#: converter/gtk/window.blp:107
msgid "Properties"
msgstr ""

#: converter/gtk/window.blp:110
msgid "Image Type"
msgstr ""

#: converter/gtk/window.blp:117
msgid "Image Size"
msgstr ""

#: converter/gtk/window.blp:126
msgid "Options"
msgstr ""

#: converter/gtk/window.blp:158
msgid "Convert to"
msgstr ""

#: converter/gtk/window.blp:163
msgid "Compress as"
msgstr ""

#: converter/gtk/window.blp:176
msgid "More Options"
msgstr ""

#: converter/gtk/window.blp:185
msgid "_Convert"
msgstr ""

#: converter/gtk/window.blp:205
msgid "Could not Load Image"
msgstr ""

#: converter/gtk/window.blp:206
msgid "This image could be corrupted or may use an unsupported file format."
msgstr ""

#: converter/gtk/window.blp:221
msgid "Quality"
msgstr ""

#: converter/gtk/window.blp:240
msgid "Background Color"
msgstr ""

#: converter/gtk/window.blp:248
msgid "DPI"
msgstr ""

#: converter/gtk/window.blp:259
msgid "Scale Preserving Ratio"
msgstr ""

#: converter/gtk/window.blp:262
msgid "Dimension"
msgstr ""

#: converter/gtk/window.blp:265 converter/gtk/window.blp:309
#: converter/gtk/window.blp:335
msgid "Width"
msgstr ""

#: converter/gtk/window.blp:265 converter/gtk/window.blp:324
#: converter/gtk/window.blp:345
msgid "Height"
msgstr ""

#: converter/gtk/window.blp:270
msgid "Width in Pixels"
msgstr ""

#: converter/gtk/window.blp:280
msgid "Height in Pixels"
msgstr ""

#: converter/gtk/window.blp:292
msgid "Resize"
msgstr ""

#: converter/gtk/window.blp:295
msgid "Filter"
msgstr ""

#: converter/gtk/window.blp:301
msgid "Size"
msgstr ""

#: converter/gtk/window.blp:304
msgid "Percentage"
msgstr ""

#: converter/gtk/window.blp:304
msgid "Exact Pixels"
msgstr ""

#: converter/gtk/window.blp:304
msgid "Minimum Pixels"
msgstr ""

#: converter/gtk/window.blp:304
msgid "Maximum Pixels"
msgstr ""

#: converter/gtk/window.blp:304
msgid "Ratio"
msgstr ""

#: converter/gtk/window.blp:312
msgid "Pixels"
msgstr ""

#: converter/gtk/window.blp:312
msgid "Preserve Ratio"
msgstr ""

#: converter/gtk/window.blp:355
msgid "Width Percentage Scale"
msgstr ""

#: converter/gtk/window.blp:365
msgid "Height Percentage Scale"
msgstr ""

#: converter/gtk/window.blp:375
msgid "Ratio Width"
msgstr ""

#: converter/gtk/window.blp:385
msgid "Ratio Height"
msgstr ""

#: converter/gtk/window.blp:405
msgid "Converting…"
msgstr ""

#: converter/gtk/window.blp:406
msgid "This could take a while."
msgstr ""

#: converter/gtk/window.blp:416 converter/window.py:531
msgid "Loading…"
msgstr ""

#: converter/gtk/window.blp:422 converter/window.py:797
msgid "_Cancel"
msgstr ""

#: converter/gtk/window.blp:448
msgid "Open File…"
msgstr ""

#: converter/gtk/window.blp:455
msgid "Show Less Popular Datatypes"
msgstr ""

#: converter/gtk/window.blp:467
msgid "Keyboard Shortcuts"
msgstr ""

#: converter/gtk/window.blp:472
msgid "About Converter"
msgstr ""

#: converter/main.py:93
msgid "Code and Design Borrowed from"
msgstr ""

#: converter/main.py:104
msgid "Sample Image from"
msgstr ""

#: converter/main.py:143
msgid ""
"Jürgen Benvenuti <gastornis@posteo.org>\n"
"Irénée Thirion <irenee.thirion@e.email>\n"
"Mattia Borda <mattiagiovanni.borda@icloud.com>\n"
"Heimen Stoffels\n"
"Sergio <sergiovg01@outlook.com>\n"
"Åke Engelbrektson <eson@svenskasprakfiler.se>\n"
"Sabri Ünal <libreajans@gmail.com>\n"
"Azat Zinnetullin"
msgstr ""

#: converter/window.py:655 converter/window.py:738
msgid "Converting Cancelled"
msgstr ""

#: converter/window.py:790
msgid "Stop converting?"
msgstr ""

#: converter/window.py:791
msgid "You will lose all progress."
msgstr ""

#: converter/window.py:798
msgid "_Stop"
msgstr ""

#: converter/window.py:831
msgid "Image converted"
msgstr ""

#: converter/window.py:832
msgid "Open"
msgstr ""

#: converter/window.py:837
msgid "Error while processing"
msgstr ""

#: converter/window.py:863
msgid "Error copied to clipboard"
msgstr ""

#: converter/window.py:867
msgid "_Copy to clipboard"
msgstr ""

#: converter/window.py:869
msgid "_Dismiss"
msgstr ""
